package orm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SubmarinesApplication {
	public static void main(String[] args) {
		SpringApplication.run(SubmarinesApplication.class, args);
	}

}
