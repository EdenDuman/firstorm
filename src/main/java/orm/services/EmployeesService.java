package orm.services;

import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import orm.models.Employee;
import orm.repository.EmloyeesRepository;

import java.util.List;

@Service
public class EmployeesService {

    @Autowired
    private EmloyeesRepository employeesRepository;

    public List<Employee> getAll() {
        return this.employeesRepository.findAll();
    }

    public Optional<Employee> getById(String id) {
        return this.employeesRepository.findById(id);
    }
}
