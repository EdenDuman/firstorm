package orm.services;

import java.sql.Date;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import orm.models.Project;
import orm.repository.ProjectRepository;

@Service
public class ProjectService {
  @Autowired
  private ProjectRepository projectRepository;

  public List<Project> getAll() {
    return this.projectRepository.findAll();
  }

  public Optional<Project> getById(String id) {
    return this.projectRepository.findById(id);
  }

  public List<Project> getByName(String name) {
    return this.projectRepository.findByName(name);
  }

  public List<Project> getByDeptNumber(String name) {
    return this.projectRepository.findByDepartmentNumber(name);
  }

  public List<Project> getShit(Date endDate, int staff) {
    return this.projectRepository.findByEndDateBeforeAndStaffLessThan(endDate, staff);
  }

  public List<Project> getGray(int staff) {
    return this.projectRepository.findByManagingProjectIsNullOrStaffGreaterThan(staff);
  }

  public List<Project> getStartWithW(String factor) {
    return this.projectRepository.findByNameStartingWith(factor);
  }
}
