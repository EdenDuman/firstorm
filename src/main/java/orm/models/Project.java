package orm.models;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name="t_proj")
public class Project {
  @Id
  @Column(name="projno")
  @JsonProperty
  private String id;

  @Column(name="projname")
  @JsonProperty
  private String name;

  @Column(name="deptno")
  @JsonProperty
  private String departmentNumber;

  @Column(name="respemp")
  @JsonProperty
  private String responsibleEmployee;

  @Column(name="prstaff")
  @JsonProperty
  private int staff;

  @Column(name="prstdate")
  @JsonProperty
  private Date startDate;

  @Column(name="prendate")
  @JsonProperty
  private Date endDate;

  @Column(name="majproj")
  @JsonProperty
  private String managingProject;
}
