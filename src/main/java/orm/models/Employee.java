package orm.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "t_empl")
public class Employee {
  @Id
  @Column(name = "empno")
  @JsonProperty
  private String id;

  @JsonProperty
  @Column(name = "firstname")
  private String firstName;

  @JsonProperty
  @Column(name = "lastname")
  private String lastName;

  @JsonProperty
  @Column(name = "deptno")
  private String departmentNumber;

  @JsonProperty
  @Column(name = "hiredate")
  private Date hireDate;

  @JsonProperty
  @Column(name = "job")
  private String job;

  @JsonProperty
  @Column(name = "edlevel")
  private int educationLevel;

  @JsonProperty
  @Column(name = "sex")
  private char sex;

  @JsonProperty
  @Column(name = "birthdate")
  private Date birthdate;

  @JsonProperty
  @Column(name = "salary")
  private int salary;

  @JsonProperty
  @Column(name = "bonus")
  private int bonus;

  @JsonProperty
  @Column(name = "comm")
  private int commission;
}
