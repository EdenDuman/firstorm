package orm.repository;

import java.sql.Date;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import orm.models.Project;

@Repository
public interface ProjectRepository extends JpaRepository<Project, String> {

  List<Project> findByName (String name);

  List<Project> findByDepartmentNumber(String name);

  List<Project> findByEndDateBeforeAndStaffLessThan(Date endDate, int staff);

  List<Project> findByManagingProjectIsNullOrStaffGreaterThan(int staff);

  List<Project> findByNameStartingWith(String factor);
}
