package orm.controllers;

import java.sql.Date;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import orm.models.Project;
import orm.services.ProjectService;

@RestController
@RequestMapping("/project")
public class ProjectController {
  @Autowired
  private ProjectService projectService;

  @GetMapping
  public List<Project> getAllProject() {
    return this.projectService.getAll();
  }

  @GetMapping("/id/{id}")
  public Optional<Project> getProjectByID(@PathVariable String id) {
    return this.projectService.getById(id);
  }

  @GetMapping("/name/{name}")
  public List<Project> getProjectByName(@PathVariable String name) {
    return this.projectService.getByName(name);
  }

  @GetMapping("/dept/{id}")
  public List<Project> getProjectDeptNumber(@PathVariable String id) {
    return this.projectService.getByDeptNumber(id);
  }

  @GetMapping("/shit")
  public List<Project> getShit() {
    return this.projectService.getShit(Date.valueOf("1983-01-01"), 2);
  }

  @GetMapping("/gray")
  public List<Project> getGray() {
    return this.projectService.getGray(5);
  }

  @GetMapping("/start")
  public List<Project> getStart() {
    return this.projectService.getStartWithW("W");
  }
}
