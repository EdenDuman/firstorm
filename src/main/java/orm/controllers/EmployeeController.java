package orm.controllers;

import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import orm.models.Employee;
import orm.services.EmployeesService;

@RestController
@RequestMapping("/employees")
public class EmployeeController {
  @Autowired private EmployeesService employeeService;

  @GetMapping
  public List<Employee> getAllEmployees() {
    return this.employeeService.getAll();
  }

  @GetMapping("/{id}")
  public Optional<Employee> getEmployeesByID(@PathVariable String id) {
    return this.employeeService.getById(id);
  }
}
